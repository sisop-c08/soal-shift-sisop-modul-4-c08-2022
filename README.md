# Laporan Praktikum Modul 4
# Identitas Kelompok
### Kelompok C08
 1.  __Doanda Dresta Rahma	5025201049__
 2.  __Putu Andhika Pratama	5025201132__
 3.  __Muhammad Raihan Athallah	5025201206__

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

### Fungsi Untuk Enkripsi dan Dekripsi
```c
char atbashCipher(char c)
{
 if (c >= 'A' && c <= 'Z')
 {
  return 'Z' - (c - 'A');
 }
 return -1;
}

char rot13(char c)
{
 if (c >= 'a' && c <= 'm')
 {
  return c + 13;
 }
 else if (c >= 'n' && c <= 'z')
 {
  return c - 13;
 }
 return -1;
}

char vigenereCipherEncode(char c, int i)
{
 int addition = vigenereKey[i % vigenereKeyLength] - 'A';
 if (c >= 'a' && c <= 'z')
 {
  return (c - 'a' + addition) % 26 + 'a';
 }
 else if (c >= 'A' && c <= 'Z')
 {
  return (c - 'A' + addition) % 26 + 'A';
 }
 return c;
}

char vigenereCipherDecode(char c, int i)
{
 int addition = vigenereKey[i % vigenereKeyLength] - 'A';
 if (c >= 'a' && c <= 'z')
 {
  return (c - 'a' + 26 - addition) % 26 + 'a';
 }
 else if (c >= 'A' && c <= 'Z')
 {
  return (c - 'A' + 26 - addition) % 26 + 'A';
 }
 return c;
}

void convertBinerToDecimal(char decimal[], char binary[])
{
 unsigned long long int decimalValue = 0;
 int length = strlen(binary);
 for (int i = 0; i < length; i++)
 {
  decimalValue <<= 1;
  decimalValue += binary[i] - '0';
 }
 sprintf(decimal, "%d", decimalValue);
}

void convertDecimalToBinary(char binary[], char decimal[])
{
 unsigned long long int decimalValue = 0;
 int length = strlen(decimal);
 for (int i = 0; i < length; i++)
 {
  decimalValue *= 10;
  decimalValue += decimal[i] - '0';
 }

 length = 0;
 for (length = 0; decimalValue > 0; length++)
 {
  binary[length] = decimalValue % 2 + '0';
  decimalValue >>= 1;
 }

 char temp;
 for (int i = 0; i < length/2; i++)
 {
  temp = binary[i];
  binary[i] = binary[length - 1 - i];
  binary[length - 1 - i] = temp;
 }

 binary[length] = '\0';
}
```

### Fungsi Untuk Mengecek Sub-string
```c
int getEncryptionType(const char *path, int *offset)
{
 int encryptionType = -1;
 if (strncmp(path + *offset, "/Animeku_", 9) == 0)
 {
  encryptionType = ANIMEKU;
 }
 else if (strncmp(path + *offset, "/IAN_", 5) == 0)
 {
  encryptionType = IAN;
 }
 else if (strncmp(path + *offset, "/nam_do-saq_", 12) == 0)
 {
  encryptionType = NAM_DO_SAQ;
 }

 char *pos = strstr(path + *offset + 1, "/");
 if (pos != NULL)
 {
  *offset = pos - path;
 }
 else
 {
  *offset = -1;
 }
 return encryptionType;
}

```

### Fungsi Untuk Dekripsi Path
```c
void decryptText(char *str, int startIndex, int endIndex, int encryptionType)
{
 int vigenereIndex = 0;

 if (encryptionType != NAM_DO_SAQ && encryptionType != -1)
 {
  for (int i = startIndex; i < endIndex; i++)
  {
   if (encryptionType == ANIMEKU)
   {
    if (str[i] >= 'a' && str[i] <= 'z')
    {
     str[i] = rot13(str[i]);
    }
    else if (str[i] >= 'A' && str[i] <= 'Z')
    {
     str[i] = atbashCipher(str[i]);
    }
   }
   else if (encryptionType == IAN)
   {
    str[i] = vigenereCipherDecode(str[i], vigenereIndex);
    vigenereIndex++;
   }
  }
 }
 else if (encryptionType == NAM_DO_SAQ)
 {
  if (endIndex != -1)
  {
   char binary[128];
   convertDecimalToBinary(binary, str + endIndex);
   int length = strlen(binary);
   for (int i = length-1; i >= 0; i--)
   {
    if (binary[i] == '1')
    {
     str[startIndex] = str[startIndex] - 'A' + 'a';
    }
    startIndex--;
   }
  }
 }
}

void decryptFile(char *str, int startIndex, int endIndex, int encryptionType)
{
 int fileExtensionPos = endIndex - 1;
 if (encryptionType == ANIMEKU)
 {
  while(fileExtensionPos >= startIndex && str[fileExtensionPos] != '.')
  {
   fileExtensionPos--;
  }
  if (fileExtensionPos < startIndex)
  {
   decryptText(str, startIndex, endIndex, encryptionType);
  }
  else
  {
   decryptText(str, startIndex, fileExtensionPos + 1, encryptionType);
  }
 }
 else if (encryptionType == IAN)
 {
  decryptText(str, startIndex, endIndex, encryptionType);
 }
 else if (encryptionType == NAM_DO_SAQ)
 {
  int fileExtensionPosDecimalCode = endIndex - 1;
  while(fileExtensionPosDecimalCode >= startIndex && str[fileExtensionPosDecimalCode] != '.')
  {
   fileExtensionPosDecimalCode--;
  }

  fileExtensionPos = fileExtensionPosDecimalCode - 1;
  while(fileExtensionPos >= startIndex && str[fileExtensionPos] != '.')
  {
   fileExtensionPos--;
  }

  if (fileExtensionPosDecimalCode < startIndex)
  {
   decryptText(str, startIndex, -1, encryptionType);
  }
  else if (fileExtensionPos < 0)
  {
   decryptText(str, fileExtensionPosDecimalCode - 1, fileExtensionPosDecimalCode + 1, encryptionType);
  }
  else
  {
   decryptText(str, fileExtensionPos - 1, fileExtensionPosDecimalCode + 1, encryptionType);
   str[fileExtensionPosDecimalCode] = '\0';
  }
 }
}

void decodeDirectoryPath(const char *path, int offset, int length, int encryptionType)
{
 char *slashPos = NULL;

 if (offset < length)
 {
  if ((slashPos = strstr(path + offset, "/")) != NULL)
  {
   if (encryptionType == NAM_DO_SAQ)
   {
    decryptText(path, offset, -1, encryptionType);
   }
   else
   {
    decryptText(path, offset, slashPos - path, encryptionType);
   }
  }
  else
  {
   decryptFile(path, offset, length, encryptionType);
  }
 }
}

int decodePath(char *filePath, const char *path)
{
 int encryptionType = -1;
 int pathStringLength = strlen(path);

 if (strcmp(path, "/") == 0)
 {
  sprintf(filePath, "%s", rootPath);
 }
 else
 {
  int offset = 0;
  while(offset != -1)
  {
   decodeDirectoryPath(path, offset + 1, pathStringLength, encryptionType);

   int nextEncryption = getEncryptionType(path, &offset);
   if (encryptionType == -1 || nextEncryption != -1)
   {
    encryptionType = nextEncryption;
   }
  }
  sprintf(filePath, "%s%s", rootPath, path);
 }

 return encryptionType;
}

}

```
### Fungsi Untuk Enkripsi String Path
```c
void encryptText(char *str, int startIndex, int endIndex, int encryptionType)
{
 int vigenereIndex = 0;
 if (encryptionType != NAM_DO_SAQ)
 {
  for (int i = startIndex; i < endIndex; i++)
  {
   if (encryptionType == ANIMEKU)
   {
    if (str[i] >= 'a' && str[i] <= 'z')
    {
     str[i] = rot13(str[i]);
    }
    else if (str[i] >= 'A' && str[i] <= 'Z')
    {
     str[i] = atbashCipher(str[i]);
    }
   }
   else if (encryptionType == IAN)
   {
    str[i] = vigenereCipherEncode(str[i], vigenereIndex);
    vigenereIndex++;
   }
  }
 }
 else
 {
  char biner[endIndex - startIndex + 1];
  for (int i = 0; i < endIndex - startIndex; i++)
  {
   if (str[i] >= 'a' && str[i] <= 'z')
   {
    str[i] = str[i] - 'a' + 'A';
    biner[i] = '1';
   }
   else
   {
    biner[i] = '0';
   }
  }
  biner[endIndex - startIndex] = '\0';
  char decimal[128];
  convertBinerToDecimal(decimal, biner);
  strcat(str, ".");
  strcat(str, decimal);
 }
}

void encryptFile(char *str, int startIndex, int endIndex, int encryptionType)
{
 int fileExtensionPos = endIndex - 1;
 if (encryptionType == ANIMEKU || encryptionType == NAM_DO_SAQ)
 {
  while(fileExtensionPos >= startIndex && str[fileExtensionPos] != '.')
  {
   fileExtensionPos--;
  }
  if (fileExtensionPos < startIndex)
  {
   encryptText(str, startIndex, endIndex, encryptionType);
  }
  else
  {
   encryptText(str, startIndex, fileExtensionPos, encryptionType);
  }
 }
 else if (encryptionType == IAN)
 {
  encryptText(str, startIndex, endIndex, encryptionType);
 }
}

```
### Fungsi Untuk Membuat Log 
```c
void writeLog(char level[], char operand[], char arg1[], char arg2[])
{
 FILE *fileWriterInnuLogPath = fopen(innuLogPath, "a");
 time_t currentTime = time(NULL);
 struct tm currentLocalTime = *localtime(&currentTime);
 char logText[256];

 sprintf(logText, "%s::%02d%02d%04d-%02d:%02d:%02d::%s",
  level,
  currentLocalTime.tm_mday,
  currentLocalTime.tm_mon,
  currentLocalTime.tm_year + 1900,
  currentLocalTime.tm_hour,
  currentLocalTime.tm_min,
  currentLocalTime.tm_sec,
  operand
 );

 if (strlen(arg1) != 0)
 {
  strcat(logText, "::");
  strcat(logText, arg1);
 }

 if (strlen(arg2) != 0)
 {
  strcat(logText, "::");
  strcat(logText, arg2);
 }
 fprintf(fileWriterInnuLogPath, "%s\n", logText);

 fclose(fileWriterInnuLogPath);
}

```
### Fungsi Untuk Implementasi Read directory
```c
static int fuse_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
 char filePath[1024];
 int encryptionType = decodePath(filePath, path);

 DIR *directory;
 struct dirent *dirData;
 struct stat st;
 char fileName[1024];

 (void) offset;
 (void) fi;

 directory = opendir(filePath);

 if (directory == NULL)
 {
  return -errno;
 }

 while((dirData = readdir(directory)) != NULL)
 {
  memset(&st, 0, sizeof(st));

  st.st_ino = dirData->d_ino;
  st.st_mode = dirData->d_type << 12;


  strcpy(fileName, filePath);
  strcat(fileName, "/");
  strcat(fileName, dirData->d_name);
  if (isRegularFile(fileName))
  {
   strcpy(fileName, dirData->d_name);
   encryptFile(fileName, 0, strlen(fileName), encryptionType);
  }
  else
  {
   strcpy(fileName, dirData->d_name);
   if (encryptionType != NAM_DO_SAQ)
   {
    encryptText(fileName, 0, strlen(fileName), encryptionType);
   }
  }

  if (filler(buf, fileName, &st, 0)) break;
 }

 closedir(directory);

 return 0;
}

```
